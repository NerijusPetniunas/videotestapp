
import Foundation
import Combine

enum Query: String, CaseIterable {
    case animals
    case nature
    case people
    case ocean
    case food
}

class VideoManager: ObservableObject {
    @Published private(set) var videos: [Video] = []
    @Published var selectedQuery: Query = Query.nature {
        didSet {
            Task.init {
                await findVideos(topic: selectedQuery)
            }
        }
    }
    
    init() {
        Task.init {
            await findVideos(topic: selectedQuery)
        }
    }
    
    func findVideos(topic: Query) async {
        do {
            
            //check is url valid
            guard let url = URL(string: "https://api.pexels.com/videos/search?query=\(topic)&per_page=10&orientation=portrait") else { fatalError("wrong/missing url") }
            
            //send my authorication header ["Authorization" : "my key"]
            var urlRequest = URLRequest(url: url)
            urlRequest.setValue("563492ad6f91700001000001cda1120e38a646979075c52b020433c7", forHTTPHeaderField: "Authorization")
            
            
            let (data, response) =  try await URLSession.shared.data(for: urlRequest)
            
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {fatalError("error while fetrhing data")}
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let decodedData = try decoder.decode(ResponseBody.self, from: data)
            self.videos = []
            self.videos = decodedData.videos
          //  print("video array: \(self.videos)")
        } catch {
            print("error fetching data \(error)")
        }
        
    }
}

//Data Struct for decoding data
struct ResponseBody: Decodable {
    var page: Int
    var perPage: Int
    var totalResults: Int
    var url: String
    var videos: [Video]
    
}

struct Video: Identifiable, Decodable {
    var id: Int
    var image: String
    var duration: Int
    var user: User
    var videoFiles: [VideoFile]
    
    struct User: Identifiable, Decodable {
        var id: Int
        var name: String
        var url: String
    }
    
    struct VideoFile: Identifiable, Decodable {
        var id: Int
        var quality: String
        var fileType: String
        var link: String
    }
}
