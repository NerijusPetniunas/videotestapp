
import SwiftUI

struct ContentView: View {
    @StateObject var videoManager = VideoManager()
    var columns = [GridItem(.adaptive(minimum: 160), spacing: 20)]
    var body: some View {
        NavigationView {
            VStack{
                HStack {
                    ForEach(Query.allCases, id: \.self) {
                        searcQuery in
                        QueryTag(query: searcQuery , isSelected: videoManager.selectedQuery == searcQuery)
                            .onTapGesture {
                                videoManager.selectedQuery  = searcQuery
                            }
                    }
                }
                
                ScrollView {
                    if videoManager.videos.isEmpty {
                        ProgressView()
                    } else {
                        LazyVGrid(columns: columns, spacing: 20) {
                            ForEach (videoManager.videos, id: \.id) { video in
                                NavigationLink {
                                    VideoView(video: video)
                                } label: {
                                    VideoCard(video: video)
                                    
                                }
                            }
                        }
                        .padding()
                    }
                }
            }
            .background(Color("AccentColor"))
            .navigationBarHidden(true)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
